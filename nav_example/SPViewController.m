//
//  SPViewController.m
//  nav_example
//
//  Created by specktro on 22/01/14.
//  Copyright (c) 2014 specktro. All rights reserved.
//

#import "SPViewController.h"
#import "SPDetailViewController.h"

static NSString *venuesURL = @"https://api.foursquare.com/v2/venues/search?radius=800&locale=es&v=20140121&client_secret=OV3JVHFAU32WALYUZ1PJOHUDU2PXP5E5CUEQC2QJC5JAKWPO&client_id=QZVLKFPQ5MNXF3ISLUNYVBTEENWKEMZ3FZMEOKEAIYW1FWLS";

@interface SPViewController () <UITableViewDataSource, UITableViewDelegate, CLLocationManagerDelegate>

@property (strong, nonatomic) NSArray *venues;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) UIRefreshControl *refreshControl;
@property (strong, nonatomic) NSIndexPath *lastSelectedIndexPath;

@property (nonatomic, readonly) CLLocationManager *locationManager;
@property (strong, nonatomic) CLLocation *lastLocation;

@end

@implementation SPViewController

@synthesize locationManager = _locationManager;

#pragma mark - Getter Selectors
- (CLLocationManager *)locationManager {
    if (!_locationManager) {
        _locationManager = [[CLLocationManager alloc] init];
        _locationManager.desiredAccuracy = kCLLocationAccuracyBest;
        _locationManager.headingFilter = kCLHeadingFilterNone;
        _locationManager.distanceFilter = 20.0;
        _locationManager.delegate = self;
    }
    
    return _locationManager;
}

#pragma mark - Private Selectors
- (void)customizeViewInterface {
    self.title = @"Venues";
    
    UIRefreshControl *refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self
                       action:@selector(refreshInfo:)
             forControlEvents:UIControlEventValueChanged];
    [self.tableView addSubview:refreshControl];
    self.refreshControl = refreshControl;
}

- (void)refreshInfo:(UIRefreshControl *)sender {
    NSString *venueURLString = [NSString stringWithFormat:@"%@&ll=%g,%g", venuesURL, self.lastLocation.coordinate.latitude, self.lastLocation.coordinate.longitude];
    NSURLSession *session = [NSURLSession sharedSession];
    [[session dataTaskWithURL:[NSURL URLWithString:venueURLString]
            completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                if (!error) {
                    NSError *jsonError;
                    self.venues = [[NSJSONSerialization JSONObjectWithData:data
                                                                   options:kNilOptions
                                                                     error:&jsonError] valueForKeyPath:@"response.venues"];
                    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                        [self.tableView reloadData];
                        [sender endRefreshing];
                    }];
                }
                else {
                    NSLog(@"Hubo un error > %@", [error localizedDescription]);
                    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                        [sender endRefreshing];
                        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Venues"
                                                                        message:@"No fue posible obtener la información"
                                                                       delegate:nil cancelButtonTitle:@"OK"
                                                              otherButtonTitles:nil];
                        [alert show];
                    }];
                }
            }] resume];
}

#pragma mark - UIViewController Life Cycle Selectors
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self customizeViewInterface];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self.tableView deselectRowAtIndexPath:self.lastSelectedIndexPath animated:YES];
    [self.locationManager startUpdatingLocation];
    
    if (self.lastLocation) {
        [self.refreshControl beginRefreshing];
        [self refreshInfo:self.refreshControl];
    }
}

#pragma mark - UITableViewDataSource Selectors
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.venues.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"cellIdentifier";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (!cell)
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
    
    NSDictionary *venueInfo = self.venues[indexPath.row];
    cell.textLabel.text = venueInfo[@"name"];
    cell.detailTextLabel.text = [venueInfo valueForKeyPath:@"location.address"];
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    
    return cell;
}

#pragma mark - UITableViewDelegate Selectors
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSDictionary *selectedVenue = self.venues[indexPath.row];
    self.lastSelectedIndexPath = indexPath;
    SPDetailViewController *detailController = [[SPDetailViewController alloc] initWithNibName:@"SPDetailViewController" bundle:nil];
    detailController.venueIdentifier = selectedVenue[@"id"];
    detailController.latitude = [[selectedVenue valueForKeyPath:@"location.lat"] floatValue];
    detailController.longitude = [[selectedVenue valueForKeyPath:@"location.lng"] floatValue];
    [self.navigationController pushViewController:detailController animated:YES];
}

#pragma mark - CLLocationManagerDelegate Selectors
#pragma mark - CLLocationManagerDelegate Selectors
- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
    CLLocation *lastLocation = [locations lastObject];
    if (!self.lastLocation) {
        self.lastLocation = lastLocation;
        [self.tableView setContentOffset:CGPointMake(0.0, -90.0) animated:YES];
        [self.refreshControl beginRefreshing];
        [self refreshInfo:self.refreshControl];
    }
    else
        self.lastLocation = lastLocation;
}

@end
