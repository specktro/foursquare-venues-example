//
//  SPVenueAnnotaion.h
//  nav_example
//
//  Created by specktro on 24/01/14.
//  Copyright (c) 2014 specktro. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SPVenueAnnotaion : NSObject <MKAnnotation>

@property (nonatomic) CLLocationCoordinate2D coordinate;

@end
