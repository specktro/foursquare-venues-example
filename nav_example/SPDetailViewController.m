//
//  SPDetailViewController.m
//  nav_example
//
//  Created by specktro on 22/01/14.
//  Copyright (c) 2014 specktro. All rights reserved.
//

#import "UIImageView+AFNetworking.h"
#import "SPVenueAnnotaion.h"

static NSString *baseURL = @"https://api.foursquare.com/v2/venues/";
static NSString *params = @"v=20140121&client_secret=OV3JVHFAU32WALYUZ1PJOHUDU2PXP5E5CUEQC2QJC5JAKWPO&client_id=QZVLKFPQ5MNXF3ISLUNYVBTEENWKEMZ3FZMEOKEAIYW1FWLS";

#import "SPDetailViewController.h"

@interface SPDetailViewController () <MKMapViewDelegate>

@property (strong, nonatomic) NSDictionary *venueInfo;
@property (weak, nonatomic) IBOutlet UIImageView *categoryImageView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *addressLabel;
@property (weak, nonatomic) IBOutlet MKMapView *map;

@end

@implementation SPDetailViewController

#pragma mark - Private Selectors
- (void)customizeViewInterface {
    self.title = @"Detalle";
}

- (void)refreshInfo {
    NSString *stringVenueURL = [NSString stringWithFormat:@"%@%@?%@", baseURL, self.venueIdentifier, params];
    NSURLSession *session = [NSURLSession sharedSession];
    [[session dataTaskWithURL:[NSURL URLWithString:stringVenueURL]
            completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                if (!error) {
                    NSError *jsonError;
                    self.venueInfo = [[NSJSONSerialization JSONObjectWithData:data
                                                                      options:kNilOptions
                                                                        error:&jsonError] valueForKeyPath:@"response.venue"];
                    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                        [self putVenueInInterface:self.venueInfo];
                    }];
                }
                else {
                    NSLog(@"Hubo un error > %@", [error localizedDescription]);
                    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Venues"
                                                                        message:@"No fue posible obtener la información"
                                                                       delegate:nil cancelButtonTitle:@"OK"
                                                              otherButtonTitles:nil];
                        [alert show];
                    }];
                }
            }] resume];
}

- (void)putVenueInInterface:(NSDictionary *)venueInfo {
    // Get image category
    NSArray *venueCategories = venueInfo[@"categories"];
    NSDictionary *category = [[venueCategories filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"primary == 1"]] lastObject];
    NSString *categoryIconURLString = [NSString stringWithFormat:@"%@bg_%d%@", [category valueForKeyPath:@"icon.prefix"], 88, [category valueForKeyPath:@"icon.suffix"]];
    [self.categoryImageView setImageWithURL:[NSURL URLWithString:categoryIconURLString] placeholderImage:[UIImage imageNamed:@"place_holder"]];
    
    // Get name
    self.nameLabel.text = venueInfo[@"name"];
    
    // Get direction
    NSDictionary *locationInfo = venueInfo[@"location"];
    NSMutableString *addressString = [@"" mutableCopy];
    NSString *street = locationInfo[@"address"];
    NSString *crossStreet = locationInfo[@"crossStreet"];
    
    [addressString appendFormat:@"%@", street.length ? street : @""];
    
    if (addressString.length && crossStreet.length)
        [addressString appendString:@" esq. "];
    
    [addressString appendFormat:@"%@", crossStreet.length ? crossStreet : @""];
    
    if (!addressString.length) {
        NSString *state = locationInfo[@"state"];
        if (state)
            [addressString appendFormat:@"%@, %@", locationInfo[@"country"], locationInfo[@"state"]];
        else
            [addressString appendFormat:@"%@", locationInfo[@"country"]];
    }
    
    self.addressLabel.text = addressString;
}

#pragma mark - UIViewController Life Cycle Selectors
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self customizeViewInterface];
    
    if (!self.venueInfo)
        [self refreshInfo];
    else
        [self putVenueInInterface:self.venueInfo];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    CLLocationCoordinate2D coordinate = CLLocationCoordinate2DMake(self.latitude, self.longitude);
    MKCoordinateRegion region = MKCoordinateRegionMake(coordinate, MKCoordinateSpanMake(0.006f, 0.006f));
    [self.map setRegion:region animated:YES];
    
    SPVenueAnnotaion *annotation = [[SPVenueAnnotaion alloc] init];
    annotation.coordinate = coordinate;
    [self.map addAnnotation:annotation];
}

#pragma mark - MKMapViewDelegate Selectors
- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation {
    if ([annotation isKindOfClass:[MKUserLocation class]])
        return nil;
    
    static NSString *annotationIdentifier = @"venueAnnotationIdentifier";
    MKPinAnnotationView *annotationView = (MKPinAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:annotationIdentifier];
    
    if (!annotationView) {
        annotationView = [[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:annotationIdentifier];
        annotationView.pinColor = MKPinAnnotationColorRed;
    }
    else
        annotationView.annotation = annotation;
    
    return annotationView;
}

@end
