//
//  SPDetailViewController.h
//  nav_example
//
//  Created by specktro on 22/01/14.
//  Copyright (c) 2014 specktro. All rights reserved.
//

@interface SPDetailViewController : UIViewController

@property (strong, nonatomic) NSString *venueIdentifier;
@property (assign, nonatomic) CGFloat latitude;
@property (assign, nonatomic) CGFloat longitude;

@end
