//
//  SPAppDelegate.h
//  nav_example
//
//  Created by specktro on 22/01/14.
//  Copyright (c) 2014 specktro. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SPAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
